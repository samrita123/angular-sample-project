import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeExistingComponent } from './change-existing.component';

describe('ChangeExistingComponent', () => {
  let component: ChangeExistingComponent;
  let fixture: ComponentFixture<ChangeExistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeExistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeExistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
